console.log('connected');

// cube of a number
let getCubed = 5**3 ;

//printing out using template literals
console.log(`The cube of 5 is ${getCubed} `);

let address = [25, 'Blue Close', 'Addington', 'Zimbabwe'];

let [houseNumber, street, town, country] = address;


//printing oout full address

console.log(`My residential address is house number ${houseNumber}, on street ${street} in the town ${town}, in a country called ${country}`);

// animal object
let animal = {
	animalType:'dog',
	tail:'long',
	furColor: 'brown',
	foodType:'Carnivore'
};

// destructuring  animal array

let {tail, foodType, furColor, animalType} = animal;

console.log(`the animal is a ${animalType} with fur color of ${furColor} which eats ${foodType}  and has a ${tail} tail`);

// numbbers array

let numbers = [5, 4, 3, 2, 1];

numbers.forEach((number)=>{
    console.log(number);
});

//reduce array

let reducedNumber = numbers.reduce((oneNum, twoNum) => oneNum + twoNum);

console.log(reducedNumber);




